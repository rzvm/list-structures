package array;

public class Main {
    public static void main(String[] args) {

        MyArrayList myArrayList1 = new MyArrayList();
        myArrayList1.add(4);
        myArrayList1.add(7);
        myArrayList1.add(3);
        myArrayList1.add(9);
        myArrayList1.add(11);
        myArrayList1.add(21);
        myArrayList1.add(17);
        myArrayList1.add(9);
        myArrayList1.add(2);
        myArrayList1.add(4);
        myArrayList1.add(89);
        myArrayList1.add(45);
        myArrayList1.add(20);
        myArrayList1.add(23);
        myArrayList1.add(99);
        myArrayList1.add(34);
        myArrayList1.showArray();

        System.out.println(myArrayList1.getAt(7));
        Integer exampleValue = myArrayList1.getAt(45);
        System.out.println(exampleValue);
    }

}
